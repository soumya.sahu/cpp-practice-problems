#include <iostream>

using namespace std;

class Pasta {

    public:
    void selectPasta(){
        cout << "Penne pasta is selected. ";
    }

    void BoilPasta(){
      cout << "Pasta is now boiling. " ;
    }

    void PutIngredients(){
        cout << "Added sauce, tomato, onion, salt and pepper" << endl;
    }

};

class SauceType: public Pasta
{
    public:
    void WhiteSaucePasta(){
    cout << "Use white sauce for making Pasta. ";
    }

    void PutIngredients(){
        cout << "Added sauce, mushrooms, onion, salt and pepper. No tomato" << endl;
    }
};
int main(){
    cout << "Class attributes" << endl;
    Pasta SimplePasta;
    SimplePasta.BoilPasta();
    SimplePasta.PutIngredients();

    cout << "Subclass attributes" << endl;
    SauceType MushroomWhitePasta;  //Inherited the subclass SauceType from the class Pasta

    MushroomWhitePasta.BoilPasta(); //Inherited the subclass SauceType from the class Pasta
    MushroomWhitePasta.WhiteSaucePasta();
    MushroomWhitePasta.PutIngredients();

    return 0;
}
