#include <iostream>

using namespace std;

class Movie {
    private:
    double movie_rating;
    public:
    string movie_title;
    string movie_director;


    Movie(string title, string director, double rating){
        movie_title = title;
        movie_director = director;
        movie_rating = rating;
        cout << "Object created for " << title << endl;
    }

    void setRating(double rating){
    if(rating < 0 || rating > 10){
       movie_rating = 0;
    }
    else
        movie_rating = rating;
    }

    double getrating(){
    return movie_rating;
    }

    };

int main()
{
    Movie harry_potter("Harry Potter", "Chris Columbus", 3);
    Movie titanic("Titanic","James Cameron" , 9);
    harry_potter.setRating(9.9);

    cout << "Movie harry potter rating " << harry_potter.getrating() << endl;
    cout << "Movie titanic rating " << titanic.getrating() << endl;


    titanic.setRating(30);
     cout << "Movie titanic rating " << titanic.getrating() << endl;


    return 0;
}
